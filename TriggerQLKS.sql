﻿use QLKS
go
--trigger after insert thuê phòng--
CREATE TRIGGER trg_AfterInsert_ThuePhong 
ON ThuePhong
For insert
AS 
begin
	declare @maPhong varchar(10)=(select MaPhong from inserted)
	declare @tinhTrang bit=(select TinhTrang from Phong where MaPhong=@maPhong)
	if (@tinhTrang=0)
	begin
		update Phong set TinhTrang=1 where MaPhong=@maPhong
	end
end 
go
----trigger after update thuê phòng--
CREATE TRIGGER trg_AfterUpdate_ThuePhong 
ON ThuePhong
For UPDATE
AS 
begin
	declare @maPhong varchar(10)=(select MaPhong from inserted)
	declare @maPhongDelete varchar(10)=(select MaPhong from deleted)
	declare @tinhTrang bit=(select TinhTrang from Phong where MaPhong=@maPhong)
	if (@tinhTrang=0)
	begin
		update [dbo].[Phong] set [Phong].TinhTrang=1 where  Phong.MaPhong=@maPhong
		update [dbo].[Phong] set [Phong].TinhTrang=0 where  Phong.MaPhong=@maPhongDelete
	end
end 
go

--trigger delete thuê phòng--
CREATE TRIGGER trg_AfterDelete_ThuePhong ON  ThuePhong
FOR DELETE
AS  
begin
	set nocount on
	declare @maPhong varchar(10)=(select MaPhong from deleted)
	update Phong set TinhTrang=0 where MaPhong=@maPhong
end
go

--trigger delete chi tiết thuê phòng--
create trigger trg_AfterDelete_ChiTietThuePhong on ChiTietThuePhong
For Delete
as
begin
	declare @maThuePhongDeleted int=(select MaThuePhong from deleted)
	delete from ThuePhong where MaThuePhong=@maThuePhongDeleted
end
go

--trigger after insert chi tiết hóa đơn--
create trigger tg_AfterInsert_CTHD on ChiTietHoaDon
for insert
as
begin			
	declare @maHD int=(select MaHD from inserted)
	declare @maThuePhong int=(select MaThuePhong from inserted)
	declare @maPhongTP int=(select MaPhong from ThuePhong where MaThuePhong=@maThuePhong)
	declare @maPhong int=(select MaPhong from Phong where Phong.MaPhong=@maPhongTP)
	declare @maLoai int =(select MaLoai from Phong where MaPhong=@maPhong)
	declare @donGia decimal=(select DonGia from LoaiPhong where MaLoai=@maLoai)
	declare @ngayBatDauThue datetime=(select NgayBatDauThue from ThuePhong where MaThuePhong=@maThuePhong)
	declare @soLuongKhach int=(Select SoLuongKhach from ChiTietThuePhong where MaThuePhong=@maThuePhong)
	declare @maKhachHang int=(select MaKhachHang from HD where MaHD=@maHD)
	declare @maLoaiKhach int=(select MaLoaiKhach from KhachHang where MaKhachHang=@maKhachHang)
	declare @heSo float=(select HeSo from LoaiKhach where MaLoaiKhach=@maLoaiKhach)
	--declare @maLoaiKhach int=(select LoaiKhach.MaLoaiKhach from LoaiKhach,KhachHang where LoaiKhach.MaLoaiKhach=KhachHang.MaLoaiKhach and MaKhachHang=@maKhachHang)
	declare @tenLoaiKhach nchar(20)=(select TenLoaiKhach from LoaiKhach where MaLoaiKhach=@maLoaiKhach)
	declare @soNgayThue int = (select SoNgayThue from inserted)
	declare @thanhTien decimal =@dongia*@soNgayThue
	if (@soLuongKhach=2 or @soLuongKhach=1 and @tenLoaiKhach='Trong Nu?c')
	begin
		update ChiTietHoaDon set ThanhTien=@thanhTien*@heSo
		,NgayThanhToan=@ngayBatDauThue+SoNgayThue 
		where MaThuePhong=@maThuePhong
		update Phong set TinhTrang=0 where  MaPhong=@maPhong
		update HD set TongTien=(select sum(ThanhTien) from ChiTietHoaDon where MaThuePhong=@maThuePhong and MaKhachHang=@maKhachHang) where MaHD=@maHD
	end
	else if (@soLuongKhach=2 or @soLuongKhach=1 and @tenLoaiKhach='Ngoài Nu?c')
	begin
		update ChiTietHoaDon set ThanhTien=@thanhTien*@heSo
		,NgayThanhToan=@ngayBatDauThue+SoNgayThue 
		where MaThuePhong=@maThuePhong
		update Phong set TinhTrang=0 where MaPhong=@maPhong
		update HD set TongTien=(select sum(ThanhTien) from ChiTietHoaDon where MaThuePhong=@maThuePhong and MaKhachHang=@maKhachHang) where MaHD=@maHD
	end
	if (@soLuongKhach=3 and @tenLoaiKhach='Ngoài Nu?c')
	Begin
		 update ChiTietHoaDon set ThanhTien=(@thanhTien*@heSo)+(@donGia*0.25)
		 ,NgayThanhToan=@ngayBatDauThue+SoNgayThue 
		 where MaThuePhong=@maThuePhong
		 update Phong set TinhTrang=0 where MaPhong=@maPhong
		 update HD set TongTien=(select sum(ThanhTien) from ChiTietHoaDon where MaThuePhong=@maThuePhong and MaKhachHang=@maKhachHang) where MaHD=@maHD
	end
	else if (@soLuongKhach=3 and @tenLoaiKhach='Trong Nu?c')
	begin
		update ChiTietHoaDon set ThanhTien=(@thanhTien*@heSo)+(@donGia*0.25)
		,NgayThanhToan=@ngayBatDauThue+SoNgayThue 
		where MaThuePhong=@maThuePhong
		update Phong set TinhTrang=0 where MaPhong=@maPhong
		update HD set TongTien=(select sum(ThanhTien) from ChiTietHoaDon where MaThuePhong=@maThuePhong and MaKhachHang=@maKhachHang) where MaHD=@maHD
	end
end
go

--trigger after update chi tiết hóa đơn--
create trigger tg_AfterUpdate_ChiTietHoaDon on ChiTietHoaDon
for update
as
begin
	declare @maHD int=(select MaHD from inserted)
	declare @maThuePhong int=(select MaThuePhong from inserted)
	declare @maPhongTP int=(select MaPhong from ThuePhong where MaThuePhong=@maThuePhong)
	declare @maPhong int=(select Phong.MaPhong from Phong where Phong.MaPhong=@maPhongTP)
	declare @maThuePhongDelete int=(select MaThuePhong from deleted)
	declare @ngayThanhToan datetime=(select NgayThanhToan from inserted)
	declare @maLoai int =(select MaLoai from Phong where MaPhong=@maPhong)
	declare @maPhongTPDelete int=(select top 1 MaPhong from ThuePhong where MaThuePhong=@maThuePhongDelete)
	declare @maPhongDelete int=(select top 1 Phong.MaPhong from Phong where Phong.MaPhong=@maPhongTPDelete)
	declare @donGia decimal=(select DonGia from LoaiPhong where MaLoai=@maLoai)
	declare @soLuongKhach int=(Select SoLuongKhach from ChiTietThuePhong where MaThuePhong=@maThuePhong)
	declare @maKhachHang int=(select MaKhachHang from HD where MaHD=@maHD)
	declare @maLoaiKhach int=(select MaLoaiKhach from KhachHang where MaKhachHang=@maKhachHang)
	declare @heSo float=(select HeSo from LoaiKhach where MaLoaiKhach=@maLoaiKhach)
	--declare @maLoaiKhach int=(select LoaiKhach.MaLoaiKhach from LoaiKhach,KhachHang where LoaiKhach.MaLoaiKhach=KhachHang.MaLoaiKhach and MaKhachHang=@maKhachHang)
	declare @tenLoaiKhach nchar(20)=(select TenLoaiKhach from LoaiKhach where MaLoaiKhach=@maLoaiKhach)
	declare @ngayBatDauThue datetime=(select top 1 NgayBatDauThue from ThuePhong where ThuePhong.MaThuePhong=@maThuePhong)
	declare @soNgayThue int = (select SoNgayThue from inserted)
	declare @tinhTrang bit=(select TinhTrang from Phong where MaPhong=@maPhong)
	if (@soLuongKhach=2 or @soLuongKhach=1 and @tenLoaiKhach='Trong Nu?c' and @tinhTrang=0)
	begin
		update ChiTietHoaDon set ThanhTien=@donGia*@soNgayThue*@heSo
		,NgayThanhToan=@ngayBatDauThue+@soNgayThue
		where MaThuePhong=@maThuePhong
		update Phong set TinhTrang=0 where MaPhong=@maPhong
		update Phong set TinhTrang=1 where MaPhong=@maPhongDelete
		update HD set TongTien=(select sum(ThanhTien) from ChiTietHoaDon where MaThuePhong=@maThuePhong and MaKhachHang=@maKhachHang) where MaHD=@maHD
	end
	else if (@soLuongKhach=2 or @soLuongKhach=1 and @tenLoaiKhach='Ngoài Nu?c' and @tinhTrang=0)
	begin
		update ChiTietHoaDon set ThanhTien=@donGia*@soNgayThue*@heSo
		,NgayThanhToan=@ngayBatDauThue+@soNgayThue
		where MaThuePhong=@maThuePhong
		update Phong set TinhTrang=0 where MaPhong=@maPhong
		update Phong set TinhTrang=1 where MaPhong=@maPhongDelete
		update HD set TongTien=(select sum(ThanhTien) from ChiTietHoaDon where MaThuePhong=@maThuePhong and MaKhachHang=@maKhachHang) where MaHD=@maHD
	end
	if (@soLuongKhach=3 and @tenLoaiKhach='Ngoài Nu?c' and @tinhTrang=0)
	Begin
		 update ChiTietHoaDon set ThanhTien=@donGia*@heSo*@soNgayThue+(@donGia*0.25)
		 ,NgayThanhToan=@ngayBatDauThue+@soNgayThue 
		 where MaThuePhong=@maThuePhong
		 update Phong set TinhTrang=0 where MaPhong=@maPhong
		 update Phong set TinhTrang=1 where MaPhong=@maPhongDelete
		 update HD set TongTien=(select sum(ThanhTien) from ChiTietHoaDon where MaThuePhong=@maThuePhong and MaKhachHang=@maKhachHang) where MaHD=@maHD
	end
	else if (@soLuongKhach=3 and @tenLoaiKhach='Trong Nu?c' and @tinhTrang=0)
	begin
		update ChiTietHoaDon set ThanhTien=@donGia*@soNgayThue*@heSo+(@donGia*0.25)
		,NgayThanhToan=@ngayBatDauThue+@soNgayThue
		where MaThuePhong=@maThuePhong
		update Phong set TinhTrang=0 where MaPhong=@maPhong
		update Phong set TinhTrang=1 where MaPhong=@maPhongDelete
		update HD set TongTien=(select sum(ThanhTien) from ChiTietHoaDon where MaThuePhong=@maThuePhong and MaKhachHang=@maKhachHang) where MaHD=@maHD
	end
end
go

--trigger after delete chi tiết hóa đơn--
create trigger tg_AfterDelete_HD on ChiTietHoaDon
for delete
as
begin
	declare @maHD int=(select MaHD from deleted)
	declare @maThuePhongDelete int=(select MaThuePhong from deleted)
	declare @maPhongTPDelete int=(select MaPhong from ThuePhong where MaThuePhong=@maThuePhongDelete)
	declare @maPhongDelete int=(select Phong.MaPhong from Phong where Phong.MaPhong=@maPhongTPDelete)
	update Phong set TinhTrang=1 where MaPhong=@maPhongDelete
	delete from HD where MaHD=@maHD
end
go
----trigger doanh thu--
--create trigger tg_AfterInsert_DoanhThu on DoanhThu
--for insert,update
--as
--begin
--	declare @maDoanhThu int=(select MaDoanhThu from inserted)
--	declare @tongDoanhThu decimal=(select Sum(DoanhThu) from ChiTietDoanhThu)
--	if(@maDoanhThu is not null)
--	begin
--		update DoanhThu set TongDoanhThu=@tongDoanhThu where MaDoanhThu=@maDoanhThu
--	end
--end
--go
--Trigger chi tiết doanh thu--
create trigger tg_AfterInsert_CTDT on ChiTietDoanhThu
for insert
as
begin
	declare @maDoanhThuCT int=(select MaDoanhThu from inserted)
	declare @maDoanhThu int=(select MaDoanhThu from DoanhThu where MaDoanhThu=@maDoanhThuCT)
	declare @thang datetime=(select Thang from DoanhThu where MaDoanhThu=@maDoanhThu)
	declare @maLoai int=(select MaLoai from inserted)
	declare @xetThang datetime=Month(getdate())
	declare @tien decimal=(select Sum(ThanhTien) from ChiTietHoaDon,Phong,ThuePhong where ChiTietHoaDon.MaThuePhong=ThuePhong.MaThuePhong and ThuePhong.MaPhong=Phong.MaPhong and Phong.MaLoai=@maLoai and MONTH(NgayThanhToan)=MONTH(@thang) group by Phong.MaLoai,Month(ChiTietHoaDon.NgayThanhToan))
	if(@maLoai=1) --and MONTH(@thang)=@xetThang)
	begin
		update ChiTietDoanhThu set DoanhThu=@tien
		,Tile=@tien*100/(select Sum(ThanhTien) from ChiTietHoaDon where MONTH(NgayThanhToan)=MONTH(@thang) group by MONTH(NgayThanhToan))
		,Thang=@thang 
		where MaDoanhThu=@maDoanhThuCT
	end
	else if(@maLoai=2) --and MONTH(@thang)=@xetThang)
	begin
		update ChiTietDoanhThu set DoanhThu=@tien
		,Tile=@tien*100/(select Sum(ThanhTien) from ChiTietHoaDon where MONTH(NgayThanhToan)=MONTH(@thang) group by MONTH(NgayThanhToan))
		,Thang=@thang 
		where MaDoanhThu=@maDoanhThuCT
	end
	else if(@maLoai=3) --and MONTH(@thang)=@xetThang)
	begin
		update ChiTietDoanhThu set DoanhThu=@tien
		,Tile=@tien*100/(select Sum(ThanhTien) from ChiTietHoaDon where MONTH(NgayThanhToan)=MONTH(@thang) group by MONTH(NgayThanhToan))
		,Thang=@thang 
		where MaDoanhThu=@maDoanhThuCT
	end
end
go

--trigger after delete chi tiết doanh thu--
create trigger trg_AfterDelete_CTDT on ChiTietDoanhThu
for delete
as
begin
	declare @maDoanhThu int=(select MaDoanhThu from deleted)
	delete from DoanhThu where MaDoanhThu=@maDoanhThu
end 

--Views Phòng--
go
create view XemPhong_ConTrong as
select MaPhong,TenPhong,TinhTrang

from Phong
where TinhTrang=0
go
--View bao cao doanh thu--
go
create view BaoCaoDoanhThu
as
select LoaiPhong,Sum(ThanhTien) as 'Tong Tien',Sum(ThanhTien) *100/
(select Sum(ThanhTien)
from HD) as'Ty Le'
from HD,ThuePhong,Phong,DoanhThu
where HD.MaThuePhong=ThuePhong.MaThuePhong and ThuePhong.MaPhong=Phong.MaPhong 
group by Phong.LoaiPhong,DoanhThu.Thang
go

select * from BaoCaoDoanhThu