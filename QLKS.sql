﻿use master 
drop database QLKS 
go
create database QLKS
use QLKS
go


create table ChucVu
(
MaCV int identity(1,1) constraint ChucVu_MaCV_PK Primary Key,
TenCV nvarchar(20),
)
go

create table NV
(
MaNV int  constraint NV_MaNV_PK Primary Key,
TenNV nvarchar (20),
MaCV int constraint NV_TenCV_FK Foreign Key (MaCV) references ChucVu(MaCV),
SDTNV nvarchar (12),
DCNV nvarchar (50), 
)
go

create table TaiKhoan
(
	MaNV int constraint TaiKhoan_MaNV_PK primary key ,
	constraint TaiKhoan_MaNV_FK Foreign Key (MaNV) references NV(MaNV),
	Email nvarchar (30), 
	Pass nvarchar(15),
	Roles nvarchar(10)
)
go
create table LoaiPhong
(
MaLoai int identity(1,1) constraint MaLoai_LoaiPhong_PK primary key,
TenLoai char(1),
Dongia decimal,
)
go
create table Phong
(
MaPhong int identity(1,1) not null constraint Phong_MaPhong_PK primary key,
TenPhong nvarchar (30) not null,
MaLoai int constraint Phong_MaLoai_PK foreign key (MaLoai) references LoaiPhong(MaLoai),
GhiChu nvarchar (60),
TinhTrang bit
)
go

create table LoaiKhach
(
	MaLoaiKhach int identity(1,1) constraint MaLoaiKhach_LoaiKhach_PK primary key,
	TenLoaiKhach nvarchar(20),
	HeSo float,
)
go
create table KhachHang
(
	MaKhachHang int identity(1,1) constraint MaKhachHang_KhachHang_PK primary key,
	TenKhachHang  nvarchar(30),
	MaLoaiKhach int constraint MaLoaiKhach_LoaiKhach_FK foreign key (MaLoaiKhach) references LoaiKhach(MaLoaiKhach),
	DiaChi nvarchar(30),
	CMND nvarchar(30),
)
go
create table ThuePhong
(
	MaThuePhong int identity(1,1) not null constraint MaThuePhong_ThuePhong_PK primary key,
	MaPhong int unique not null,
	NgayBatDauThue datetime,
	constraint MaPhong_ThuePhong_FK foreign key (MaPhong) references Phong(MaPhong)
)

go
create table ChiTietThuePhong
(
	MaThuePhong int not null,
	MaKhachHang int constraint MaKhachHang_KhachHang_FK foreign key (MaKhachHang) references KhachHang(MaKhachHang),
	SoLuongKhach int,
	constraint MaThuePhong_MaKhachHang_ChiTietThuePhong_PK primary key(MaThuePhong,MaKhachHang),
	constraint MaThuePhong_ChiTietThuePhong_FK foreign key (MaThuePhong) references ThuePhong(MaThuePhong)
)
go
create table HD
(
	MaHD int identity(1,1) not null constraint HD_MaHD_PK primary key,
	MaKhachHang int constraint MaKhachHang_HD_KhachHang_FK foreign key (MaKhachHang)  references KhachHang(MaKhachHang),
	MaNV int  constraint HD_MaNV_FK Foreign Key (MaNV) references NV(MaNV),
	TongTien decimal,
)
go
create table ChiTietHoaDon
(
	MaHD int not null constraint MaHD_HD_FK foreign key (MaHD) references HD(MaHD),
	MaThuePhong int not null constraint MaPhong_FK foreign key (MaThuePhong) references ThuePhong(MaThuePhong),
	constraint HD_MaPhong_MaHD_PK Primary Key(MaHD,MaThuePhong), 
	NgayThanhToan datetime,
	SoNgayThue int,
	ThanhTien decimal,
)
go
create table DoanhThu
(
	MaDoanhThu int identity(1,1),
	Thang DateTime,
	constraint DoanhThu_MaDoanhThu_PK Primary Key (MaDoanhThu)
)
go

create table ChiTietDoanhThu 
(
	MaDoanhThu int constraint ChiTietDoanhThu_MaDoanhThu_PK Primary Key,
	MaLoai int constraint MaLoai_ChiTietDoanhThu_FK foreign key (MaLoai) references LoaiPhong(MaLoai),
	Thang datetime,
	DoanhThu decimal,
	Tile decimal,
	constraint DoanhThu_Thang_MaDoanhThu_FK Foreign Key (MaDoanhThu) references DoanhThu(MaDoanhThu),
)
go
create table QuiDinh
(
	MaQuiDinh int identity(1,1) constraint QuiDinh_MaQuiDinh_PK primary key,
	TenQuiDinh nvarchar(30),
	MoTa nvarchar(100),
	ThamSo float,
	ThamSoTien decimal,
)
go
select DatePart(MM,Thang) as Thang from DoanhThu

go
declare @Thang INT;

--Qui định--
insert into QuiDinh(TenQuiDinh,MoTa) values('Qui định 1','Thay đổi số lượng và đơn giá các loại phòng');
insert into QuiDinh(TenQuiDinh,MoTa) values('Qui định 2','Thay đổi số lượng loại khách,số lượng khách tối đa trong phòng');
insert into QuiDinh(TenQuiDinh,MoTa) values('Qui định 3','Thay đổi tỉ lệ phụ thu');



--Loại Phòng--
insert into LoaiPhong(TenLoai,Dongia) values('A',150000);
insert into LoaiPhong(TenLoai,Dongia) values('B',170000);
insert into LoaiPhong(TenLoai,Dongia) values('C',200000);


--Loại khách--
insert into LoaiKhach(TenLoaiKhach,HeSo) values ('Trong Nước',1);
insert into LoaiKhach(TenLoaiKhach,HeSo) values ('Ngoài Nước',1.5);


--Chuc vu--
insert into ChucVu (TenCV) values ('Quan Ly KS');
insert into ChucVu (TenCV) values ('Quan Ly Nhan Su');
insert into ChucVu (TenCV) values ('Ke Toan Truong');
insert into ChucVu (TenCV) values ('Tiep Tan');
insert into ChucVu (TenCV) values ('Don Phong');
insert into ChucVu (TenCV) values ('Bao Ve');
insert into ChucVu (TenCV) values ('Bartender');
insert into ChucVu (TenCV) values ('Ke Toan');
insert into ChucVu (TenCV) values ('Dau Bep');
insert into ChucVu (TenCV) values ('Phuc Vu');
insert into ChucVu (TenCV) values ('Lao Cong');
insert into ChucVu (TenCV) values ('Giu Xe');
insert into ChucVu (TenCV) values ('Bao Tri');


--NV--

insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (1, 'Ngoc', 1, '6453163578', '581 Shoshone Park');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (2, 'Yen', 2, '0152083928', '656 Goodland Hill');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (3, 'Quy', 3, '9468095088', '31 Pine View Way');


insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (004, 'Quyen', 4, '9468095088', '31 Pine View Way');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (005, 'Phuc', 5, '5552470052', '67542 Fair Oaks Parkway');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (006, 'Minh', 6, '0060010509', '95003 Fieldstone Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (007, 'Hung', 7, '2588607888', '8707 Talisman Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (008, 'Ngoc', 8, '9131540651', '4 Buena Vista Circle');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (009, 'Phuc', 9, '8399599557', '633 Ohio Junction');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (010, 'Tuyen', 10, '8077429998', '99547 Bunting Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (011, 'Loc', 13, '9502483758', '224 Rigney Road');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (012, 'Anh', 1, '5566740758', '4321 Corscot Alley');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (013, 'Mai', 2, '1664781056', '1 Rutledge Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (014, 'Tuyen', 3, '5011636429', '311 Golf Street');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (015, 'Ngoc', 4, '7623103717', '187 Ronald Regan Parkway');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (016, 'Tuyen', 5, '7947769206', '90 Almo Lane');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (017, 'Nhung', 6, '8926208117', '60 Brown Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (018, 'Lien', 7, '3185409493', '08 Drewry Alley');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (019, 'Phuc', 8, '9357237453', '466 Upham Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (020, 'Lien', 9, '0420824006', '43 Forest Dale Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (021, 'Tuyen', 10, '3951027355', '684 Mesta Way');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (022, 'Hung', 11, '9586945251', '48710 Oriole Parkway');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (023, 'Loc', 12, '3321606763', '52 Algoma Avenue');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (024, 'Yen', 13, '6084450547', '1 Bay Junction');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (025, 'Tung', 1, '5737939553', '8674 Mccormick Way');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (026, 'Quyen', 2, '3494238987', '7 Loeprich Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (027, 'Qui', 3, '0305471201', '62451 Scofield Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (028, 'Quyen', 4, '5858032974', '7751 Trailsway Park');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (029, 'Trung', 5, '9160101113', '94 Nobel Hill');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (030, 'Qui', 6, '6956497255', '58 5th Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (031, 'Tuyen', 7, '5837065288', '263 Fulton Lane');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (032, 'Mai', 8, '4401732898', '58692 Sundown Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (033, 'Loc', 9, '3071257856', '3 Bluejay Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (034, 'Tung', 10, '1476659958', '01880 Helena Court');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (035, 'Thong', 11, '4812197791', '516 Superior Alley');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (036, 'Tuyen', 12, '1931956464', '6604 Loeprich Avenue');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (037, 'Lam', 13, '7497449990', '046 Tomscot Parkway');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (038, 'Yen', 1, '8763486652', '05 Doe Crossing Park');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (039, 'Thong', 2, '1859119441', '8 Prairie Rose Circle');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (040, 'Nam', 3, '9577161383', '0 Ridgeview Court');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (041, 'Thong', 4, '3619315876', '16 Haas Hill');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (042, 'Quyen', 5, '2807013821', '320 Dryden Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (043, 'Hung', 6, '9053009779', '922 Clemons Court');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (044, 'Nhung', 7, '0441035639', '420 Jenna Way');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (045, 'Loc', 8, '7151246684', '1 John Wall Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (046, 'Phuong', 9, '0851669069', '909 Weeping Birch Lane');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (047, 'Lien', 10, '4951465647', '66627 Johnson Alley');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (048, 'Nhung', 11, '5227377006', '4 Bartillon Court');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (049, 'Thong', 12, '1748986120', '377 Longview Trail');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (050, 'Ngoc', 13, '4999493683', '06734 Esch Hill');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (051, 'Tuyen', 1, '0377891223', '6 Cascade Street');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (052, 'Tuyen', 2, '9292600346', '6 International Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (053, 'Nam', 3, '5516681464', '860 Superior Place');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (054, 'Lam', 4, '6845972338', '17603 Oneill Street');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (055, 'Thong', 5, '7981886740', '163 Mosinee Way');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (056, 'Yen', 6, '2546864874', '2 Michigan Court');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (057, 'Phuc', 7, '3091298019', '9869 Forest Run Hill');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (058, 'Quyen', 8, '1964012309', '54 Stephen Drive');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (059, 'Phuc', 9, '3326553609', '7 Spaight Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (060, 'Anh', 10, '4656612634', '4 Cascade Trail');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (061, 'Quyen', 11, '0305975579', '76 Michigan Point');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (062, 'Mai', 12, '9253572671', '0 Dunning Point');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (063, 'Ngoc', 13, '6528354367', '8 Cordelia Hill');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (064, 'Lam', 1, '9872598266', '30180 Milwaukee Park');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (065, 'Lam', 2, '5555624654', '345 Morrow Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (066, 'Lien', 3, '1680471791', '6 Clove Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (067, 'Nam', 4, '7914788783', '2244 Jay Crossing');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (068, 'Qui', 5, '6984022649', '7 Sage Circle');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (069, 'Minh', 6, '9205895377', '4393 Rieder Parkway');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (070, 'Nam', 7, '1957613386', '0941 Magdeline Place');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (071, 'Ngoc', 8, '7095399441', '20 Randy Hill');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (072, 'Tung', 9, '1722694122', '59857 Doe Crossing Drive');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (073, 'Qui', 10, '9560231154', '6 Kinsman Trail');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (074, 'Loc', 11, '7314197512', '26 Becker Place');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (075, 'Hung', 12, '8074152839', '34 Eagle Crest Trail');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (076, 'Tung', 13, '7573400802', '52 Burning Wood Trail');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (077, 'Minh', 1, '4016000033', '76 Petterle Circle');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (078, 'Phuc', 2, '1236196759', '4487 Everett Junction');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (079, 'Tuyen', 3, '1295903407', '8 Shoshone Avenue');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (080, 'Nam', 4, '6981771357', '6 Meadow Valley Drive');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (081, 'Tuyen', 5, '2635100764', '1 Bay Trail');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (082, 'Nam', 6, '5261352800', '120 Manufacturers Circle');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (083, 'Mai', 7, '1424025575', '4 Eagan Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (084, 'Thong', 8, '5912884139', '754 Portage Road');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (085, 'Nam', 9, '4130458558', '26 Atwood Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (086, 'Ngoc', 10, '6963443793', '31846 Graedel Point');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (087, 'Quyen', 11, '8507022545', '287 Tennessee Avenue');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (088, 'Phuc', 12, '5202883501', '1172 Dorton Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (089, 'Tuyen', 13, '4576795171', '171 Clemons Way');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (090, 'Phuong', 1, '5700285266', '63623 Karstens Lane');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (091, 'Trung', 2, '4482850942', '35 7th Parkway');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (092, 'Minh', 3, '1522490515', '266 Tomscot Alley');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (093, 'Tung', 4, '3709996538', '5 7th Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (094, 'Nam', 5, '9160711515', '851 Scoville Way');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (095, 'Loc', 6, '8178336235', '5 Rowland Hill');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (096, 'Nhung', 7, '8092962774', '496 Lunder Street');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (097, 'Quyen', 8, '6248141533', '92606 Arkansas Place');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (098, 'Phuong', 9, '8076910951', '9 Eagle Crest Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (099, 'Minh', 10, '1226612318', '0021 Stuart Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (100, 'Mai', 12, '8741481798', '836 East Place');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (101, 'Mai', 13, '6120251561', '28 Bunting Street');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (102, 'Minh', 9, '8370045766', '71471 Fisk Lane');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (103, 'Anh', 11, '1804479187', '31733 New Castle Circle');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (104, 'Tuyen', 11, '8233113964', '3826 Kensington Way');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (105, 'Nam', 4, '5725180050', '33261 Shelley Terrace');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (106, 'Qui', 12, '7081620655', '4 Lotheville Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (107, 'Lien', 11, '2533486124', '1220 Lakeland Street');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (108, 'Anh', 13, '0564372080', '0034 Pawling Road');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (109, 'Tuyen', 8, '8166716623', '093 Hovde Park');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (110, 'Trung', 6, '1667625489', '4461 Ramsey Point');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (111, 'Trung', 11, '8347505039', '1776 Kingsford Circle');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (112, 'Ngoc', 5, '1954110774', '7 Hollow Ridge Trail');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (113, 'Phuong', 8, '0762741104', '71877 Canary Place');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (114, 'Phuc', 7, '8317561895', '1 Prairieview Crossing');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (115, 'Minh', 5, '5355669166', '878 Artisan Lane');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (116, 'Thong', 11, '9598084906', '89 Scoville Avenue');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (117, 'Lam', 6, '6861654116', '818 Green Park');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (118, 'Tuyen', 4, '3986757651', '0 Merry Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (119, 'Lien', 4, '9349881047', '18633 Bay Street');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (120, 'Thong', 9, '2526797837', '43 Mariners Cove Junction');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (121, 'Yen', 4, '8243806237', '4160 Graceland Street');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (122, 'Quyen', 4, '6218014177', '8727 Waubesa Court');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (123, 'Nam', 10, '6430756181', '31 Dryden Parkway');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (124, 'Phuong', 5, '4681904058', '94 Mcguire Drive');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (125, 'Minh', 12, '6029393790', '83 Northland Place');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (126, 'Yen', 9, '8914304191', '33839 Monica Drive');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (127, 'Loc', 10, '0300952988', '3302 Mendota Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (128, 'Quyen', 12, '2002131686', '85313 Becker Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (129, 'Phuong', 5, '2817934199', '79 Gateway Avenue');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (130, 'Anh', 4, '9834749112', '99 Hoffman Terrace');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (131, 'Thong', 13, '3286239488', '1316 Lighthouse Bay Lane');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (132, 'Loc', 6, '0637830784', '2570 Stoughton Alley');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (133, 'Hung', 10, '6253539973', '8 Mayfield Lane');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (134, 'Lien', 7, '3261060492', '41 Spenser Parkway');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (135, 'Nam', 13, '5004993122', '0 Holy Cross Circle');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (136, 'Minh', 8, '9230098248', '9 Moland Street');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (137, 'Phuong', 13, '1291532897', '3 Schlimgen Way');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (138, 'Mai', 10, '9080429406', '094 Red Cloud Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (139, 'Hung', 9, '3809845310', '59923 Hermina Point');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (140, 'Mai', 6, '0457557494', '63744 Kensington Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (141, 'Ngoc', 7, '0527148113', '6231 Kedzie Terrace');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (142, 'Minh', 4, '5066753850', '49 Crest Line Point');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (143, 'Loc', 13, '7967459660', '4381 Moland Avenue');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (144, 'Quyen', 9, '9236376533', '347 Graceland Park');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (145, 'Thong', 4, '1655560522', '731 Erie Junction');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (146, 'Phuong', 12, '9868713714', '7741 Little Fleur Trail');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (147, 'Hung', 8, '7013914347', '06364 Hollow Ridge Way');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (148, 'Hung', 6, '6518998784', '8616 Algoma Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (149, 'Quyen', 5, '2293198375', '9098 Buena Vista Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (150, 'Quyen', 5, '9026326920', '64 Eliot Alley');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (151, 'Nhung', 4, '7446325615', '1190 Clove Junction');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (152, 'Hung', 13, '1164305484', '041 Colorado Hill');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (153, 'Thong', 8, '7289579836', '03 Canary Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (154, 'Tuyen', 4, '2296567290', '6726 Pepper Wood Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (155, 'Anh', 13, '8975109925', '7384 Comanche Court');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (156, 'Phuc', 10, '3409806970', '6 Amoth Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (157, 'Lam', 9, '8908574247', '4537 Hagan Park');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (158, 'Ngoc', 6, '5771156280', '74 Swallow Circle');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (159, 'Nhung', 10, '5594488197', '00 Declaration Circle');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (160, 'Phuc', 8, '3391238151', '765 Springview Way');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (161, 'Hung', 8, '8329275809', '00 Farragut Trail');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (162, 'Mai', 7, '5453381248', '9 Dixon Circle');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (163, 'Mai', 8, '5357883386', '92 Dennis Lane');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (164, 'Thong', 13, '8088362938', '7400 Eliot Avenue');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (165, 'Lien', 7, '0010351590', '36 Buena Vista Court');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (166, 'Phuong', 12, '3308828804', '7 Lyons Road');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (167, 'Nhung', 7, '1575051605', '4 Express Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (168, 'Lien', 9, '5801904875', '13252 Shelley Street');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (169, 'Mai', 7, '2353785875', '68 Parkside Terrace');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (170, 'Tung', 4, '6566992359', '4717 Burning Wood Hill');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (171, 'Nam', 6, '7115762279', '828 Stone Corner Junction');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (172, 'Thong', 13, '6865841719', '9 Hoepker Street');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (173, 'Anh', 4, '3924459444', '673 Anhalt Place');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (174, 'Yen', 9, '7200619523', '16591 Vermont Park');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (175, 'Minh', 9, '8641272097', '61 Anzinger Way');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (176, 'Minh', 8, '0366233491', '21149 Grover Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (177, 'Nam', 9, '7737036962', '926 Bay Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (178, 'Phuong', 4, '4177053155', '562 7th Street');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (179, 'Lien', 12, '7637712192', '207 Nancy Avenue');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (180, 'Phuc', 4, '6639203103', '2953 Northview Hill');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (181, 'Trung', 6, '3033992986', '4238 Crescent Oaks Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (182, 'Mai', 13, '6155169721', '705 Ronald Regan Terrace');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (183, 'Tuyen', 6, '6339691331', '7466 Debra Pass');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (184, 'Nhung', 10, '2509834096', '12149 Bay Place');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (185, 'Lam', 12, '0512955719', '7 Kensington Alley');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (186, 'Phuc', 8, '7990425297', '34993 Elmside Lane');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (187, 'Nam', 9, '2164843843', '208 Maywood Plaza');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (188, 'Minh', 4, '0226227103', '1978 Dawn Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (189, 'Minh', 6, '5078945296', '60 Bonner Center');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (190, 'Phuc', 4, '6758597792', '4203 Mendota Point');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (191, 'Ngoc', 6, '0878074929', '6886 East Circle');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (192, 'Yen', 7, '4437513649', '54555 Northridge Avenue');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (193, 'Lam', 11, '7639848911', '9026 Corben Place');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (194, 'Ngoc', 13, '3207191428', '9043 Carberry Junction');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (195, 'Nam', 13, '0248563246', '560 Oxford Drive');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (196, 'Minh', 5, '9899911496', '62859 Eagle Crest Drive');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (197, 'Thong', 6, '9580065829', '50 Fuller Junction');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (198, 'Thong', 4, '0177174064', '96717 Lawn Point');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (199, 'Phuong', 10, '5770880392', '2707 Farmco Point');
insert into NV (MaNV, TenNV, MaCV, SDTNV, DCNV) values (200, 'Yen', 6, '9749076869', '68 Farwell Lane');



--Tai Khoan--

insert into TaiKhoan (MaNV,Email, Pass, Roles) values (1,'admin456@gmail.com', '123456','Admin');
insert into TaiKhoan (MaNV,Email, Pass, Roles) values (2,'pm123@gmail.com', '123456', 'PM');
insert into TaiKhoan (MaNV,Email, Pass, Roles) values (3,'am123@gmail.com', '123456','AM');




--Phong--
insert into Phong values ('G01', 1, 'Phòng thường', 0);
insert into Phong values ('G02', 2, 'Phòng family', 0);
insert into Phong values ('G03', 3, 'Phòng VIP', 0);
insert into Phong values ('G04', 1, 'Phòng thường', 0);
insert into Phong values ('G05', 2, 'Phòng family', 0);
insert into Phong values ('G06', 3, 'Phòng VIP', 0);
insert into Phong values ('G07', 1, 'Phòng thường', 0);
insert into Phong values ('G08', 2, 'Phòng family', 0);
insert into Phong values ('G09', 3, 'Phòng VIP', 0);
insert into Phong values ('G10', 1, 'Phòng thường', 0);
insert into Phong values ('G11', 2, 'Phòng family', 0);
insert into Phong values ('G12', 3, 'Phòng VIP', 0);
insert into Phong values ('G13', 1, 'Phòng thường', 0);
insert into Phong values ('G14', 2, 'Phòng family', 0);
insert into Phong values ('G15', 3, 'Phòng VIP', 0);
insert into Phong values ('G16', 1, 'Phòng thường', 0);
insert into Phong values ('G17', 2, 'Phòng family', 0);
insert into Phong values ('G18', 3, 'Phòng VIP', 0);
insert into Phong values ('G19', 1, 'Phòng thường', 0);
insert into Phong values ('G20', 2, 'Phòng family', 0);
insert into Phong values ('G21', 3, 'Phòng VIP', 0);
insert into Phong values ('G22', 1, 'Phòng thường', 0);
insert into Phong values ('G23', 2, 'Phòng family', 0);
insert into Phong values ('G24', 3, 'Phòng VIP', 0);
insert into Phong values ('G25', 1, 'Phòng thường', 0);
insert into Phong values ('G26', 2, 'Phòng family', 0);
insert into Phong values ('G27', 3, 'Phòng VIP', 0);
insert into Phong values ('G28', 1, 'Phòng thường', 0);
insert into Phong values ('G29', 2, 'Phòng family', 0);
insert into Phong values ('G30', 3, 'Phòng VIP', 0);
insert into Phong values ('G31', 1, 'Phòng thường', 0);
insert into Phong values ('G32', 2, 'Phòng family', 0);
insert into Phong values ('G33', 3, 'Phòng VIP', 0);
insert into Phong values ('G34', 1, 'Phòng thường', 0);
insert into Phong values ('G35', 2, 'Phòng family', 0);
insert into Phong values ('G36', 3, 'Phòng VIP', 0);
insert into Phong values ('G37', 1, 'Phòng thường', 0);
insert into Phong values ('G38', 2, 'Phòng family', 0);
insert into Phong values ('G39', 3, 'Phòng VIP', 0);
insert into Phong values ('G40', 1, 'Phòng thường', 0);
insert into Phong values ('G41', 2, 'Phòng family', 0);
insert into Phong values ('G42', 3, 'Phòng VIP', 0);
insert into Phong values ('G43', 1, 'Phòng thường', 0);
insert into Phong values ('G44', 2, 'Phòng family', 0);
insert into Phong values ('G45', 3, 'Phòng VIP', 0);
insert into Phong values ('G46', 1, 'Phòng thường', 0);
insert into Phong values ('G47', 2, 'Phòng family', 0);
insert into Phong values ('G48', 3, 'Phòng VIP', 0);
insert into Phong values ('G49', 1, 'Phòng thường', 0);
insert into Phong values ('G50', 2, 'Phòng family', 0);
insert into Phong values ('G51', 3, 'Phòng VIP', 0);
insert into Phong values ('G52', 1, 'Phòng thường', 0);
insert into Phong values ('G53', 2, 'Phòng family', 0);
insert into Phong values ('G54', 3, 'Phòng VIP', 0);
insert into Phong values ('G55', 1, 'Phòng thường', 0);
insert into Phong values ('G56', 2, 'Phòng family', 0);
insert into Phong values ('G57', 3, 'Phòng VIP', 0);
insert into Phong values ('G58', 1, 'Phòng thường', 0);
insert into Phong values ('G59', 2, 'Phòng family', 0);
insert into Phong values ('G60', 3, 'Phòng VIP', 0);
insert into Phong values ('G61', 1, 'Phòng thường', 0);
insert into Phong values ('G62', 2, 'Phòng family', 0);
insert into Phong values ('G63', 3, 'Phòng VIP', 0);
insert into Phong values ('G64', 1, 'Phòng thường', 0);
insert into Phong values ('G65', 2, 'Phòng family', 0);
insert into Phong values ('G66', 1, 'Phòng thường', 0);
insert into Phong values ('G67', 2, 'Phòng family', 0);
insert into Phong values ('G68', 3, 'Phòng VIP', 0);
insert into Phong values ('G69', 1, 'Phòng thường', 0);
insert into Phong values ('G70', 2, 'Phòng family', 0);
insert into Phong values ('G71', 3, 'Phòng VIP', 0);
insert into Phong values ('G72', 1, 'Phòng thường', 0);
insert into Phong values ('G73', 2, 'Phòng family', 0);
insert into Phong values ('G74', 3, 'Phòng VIP', 0);
insert into Phong values ('G75', 1, 'Phòng thường', 0);
insert into Phong values ('G76', 2, 'Phòng family', 0);
insert into Phong values ('G77', 3, 'Phòng VIP', 0);
insert into Phong values ('G78', 1, 'Phòng thường', 0);
insert into Phong values ('G79', 2, 'Phòng family', 0);
insert into Phong values ('G80', 3, 'Phòng VIP', 0);
insert into Phong values ('G81', 1, 'Phòng thường', 0);
insert into Phong values ('G82', 2, 'Phòng family', 0);
insert into Phong values ('G83', 3, 'Phòng VIP', 0);
insert into Phong values ('G84', 1, 'Phòng thường', 0);
insert into Phong values ('G85', 2, 'Phòng family', 0);
insert into Phong values ('G86', 3, 'Phòng VIP', 0);
insert into Phong values ('G87', 1, 'Phòng thường', 0);
insert into Phong values ('G88', 2, 'Phòng family', 0);
insert into Phong values ('G89', 3, 'Phòng VIP', 0);
insert into Phong values ('G90', 1, 'Phòng thường', 0);
insert into Phong values ('G91', 2, 'Phòng family', 0);
insert into Phong values ('G92', 3, 'Phòng VIP', 0);
insert into Phong values ('G93', 1, 'Phòng thường', 0);
insert into Phong values ('G94', 2, 'Phòng family', 0);
insert into Phong values ('G95', 3, 'Phòng VIP', 0);
insert into Phong values ('G96', 1, 'Phòng thường', 0);
insert into Phong values ('G97', 2, 'Phòng family', 0);
insert into Phong values ('G98', 3, 'Phòng VIP', 0);
insert into Phong values ('G99', 1, 'Phòng thường', 0);
insert into Phong values ('G100', 2, 'Phòng family', 0);
insert into Phong values ('G101', 3, 'Phòng VIP', 0);
insert into Phong values ('G102', 1, 'Phòng thường', 0);
insert into Phong values ('G103', 2, 'Phòng family', 0);
insert into Phong values ('G104', 3, 'Phòng VIP', 0);
insert into Phong values ('G105', 1, 'Phòng thường', 0);
insert into Phong values ('G106', 2, 'Phòng family', 0);
insert into Phong values ('G107', 3, 'Phòng VIP', 0);
insert into Phong values ('G108', 3, 'Phòng VIP', 0);
insert into Phong values ('G109', 3, 'Phòng VIP', 0);
insert into Phong values ('G110', 3, 'Phòng VIP', 0);
insert into Phong values ('G111', 2, 'Phòng family', 0);
insert into Phong values ('G112', 3, 'Phòng VIP', 0);
insert into Phong values ('G113', 1, 'Phòng thường', 0);
insert into Phong values ('G114', 2, 'Phòng family', 0);
insert into Phong values ('G115', 3, 'Phòng VIP', 0);
insert into Phong values ('G116', 1, 'Phòng thường', 0);
insert into Phong values ('G117', 2, 'Phòng family', 0);
insert into Phong values ('G118', 3, 'Phòng VIP', 0);
insert into Phong values ('G119', 1, 'Phòng thường', 0);
insert into Phong values ('G120', 2, 'Phòng family', 0);
insert into Phong values ('G121', 3, 'Phòng VIP', 0);
insert into Phong values ('G122', 1, 'Phòng thường', 0);
insert into Phong values ('G123', 2, 'Phòng family', 0);
insert into Phong values ('G124', 3, 'Phòng VIP', 0);
insert into Phong values ('G125', 1, 'Phòng thường', 0);
insert into Phong values ('G126', 2, 'Phòng family', 0);
insert into Phong values ('G127', 3, 'Phòng VIP', 0);
insert into Phong values ('G128', 1, 'Phòng thường', 0);
insert into Phong values ('G129', 2, 'Phòng family', 0);
insert into Phong values ('G130', 3, 'Phòng VIP', 0);
insert into Phong values ('G131', 1, 'Phòng thường', 0);
insert into Phong values ('G132', 2, 'Phòng family', 0);
insert into Phong values ('G133', 3, 'Phòng VIP', 0);
insert into Phong values ('G134', 1, 'Phòng thường', 0);
insert into Phong values ('G135', 2, 'Phòng family', 0);
insert into Phong values ('G136', 3, 'Phòng VIP', 0);
insert into Phong values ('G137', 1, 'Phòng thường', 0);
insert into Phong values ('G138', 2, 'Phòng family', 0);
insert into Phong values ('G139', 3, 'Phòng VIP', 0);
insert into Phong values ('G140', 1, 'Phòng thường', 0);
insert into Phong values ('G141', 2, 'Phòng family', 0);
insert into Phong values ('G142', 3, 'Phòng VIP', 0);
insert into Phong values ('G143', 1, 'Phòng thường', 0);
insert into Phong values ('G144', 2, 'Phòng family', 0);
insert into Phong values ('G145', 3, 'Phòng VIP', 0);
insert into Phong values ('G146', 1, 'Phòng thường', 0);
insert into Phong values ('G147', 2, 'Phòng family', 0);
insert into Phong values ('G148', 3, 'Phòng VIP', 0);
insert into Phong values ('G149', 1, 'Phòng thường', 0);
insert into Phong values ('G150', 2, 'Phòng family', 0);
insert into Phong values ('G151', 3, 'Phòng VIP', 0);
insert into Phong values ('G152', 1, 'Phòng thường', 0);
insert into Phong values ('G153', 2, 'Phòng family', 0);
insert into Phong values ('G154', 3, 'Phòng VIP', 0);
insert into Phong values ('G155', 1, 'Phòng thường', 0);
insert into Phong values ('G156', 2, 'Phòng family', 0);
insert into Phong values ('G157', 3, 'Phòng VIP', 0);
insert into Phong values ('G158', 1, 'Phòng thường', 0);
insert into Phong values ('G159', 2, 'Phòng family', 0);
insert into Phong values ('G160', 3, 'Phòng VIP', 0);
insert into Phong values ('G161', 1, 'Phòng thường', 0);
insert into Phong values ('G162', 2, 'Phòng family', 0);
insert into Phong values ('G163', 3, 'Phòng VIP', 0);
insert into Phong values ('G164', 1, 'Phòng thường', 0);
insert into Phong values ('G165', 2, 'Phòng family', 0);
insert into Phong values ('G166', 3, 'Phòng VIP', 0);
insert into Phong values ('G167', 1, 'Phòng thường', 0);
insert into Phong values ('G168', 2, 'Phòng family', 0);
insert into Phong values ('G169', 3, 'Phòng VIP', 0);
insert into Phong values ('G170', 1, 'Phòng thường', 0);
insert into Phong values ('G171', 2, 'Phòng family', 0);
insert into Phong values ('G172', 3, 'Phòng VIP', 0);
insert into Phong values ('G173', 1, 'Phòng thường', 0);
insert into Phong values ('G174', 2, 'Phòng family', 0);
insert into Phong values ('G175', 3, 'Phòng VIP', 0);
insert into Phong values ('G176', 1, 'Phòng thường', 0);
insert into Phong values ('G177', 2, 'Phòng family', 0);
insert into Phong values ('G178', 3, 'Phòng VIP', 0);
insert into Phong values ('G179', 1, 'Phòng thường', 0);
insert into Phong values ('G180', 2, 'Phòng family', 0);
insert into Phong values ('G181', 3, 'Phòng VIP', 0);
insert into Phong values ('G182', 1, 'Phòng thường', 0);
insert into Phong values ('G183', 2, 'Phòng family', 0);
insert into Phong values ('G184', 3, 'Phòng VIP', 0);
insert into Phong values ('G185', 1, 'Phòng thường', 0);
insert into Phong values ('G186', 2, 'Phòng family', 0);
insert into Phong values ('G187', 3, 'Phòng VIP', 0);
insert into Phong values ('G188', 1, 'Phòng thường', 0);
insert into Phong values ('G189', 2, 'Phòng family', 0);
insert into Phong values ('G190', 3, 'Phòng VIP', 0);
insert into Phong values ('G191', 1, 'Phòng thường', 0);
insert into Phong values ('G192', 2, 'Phòng family', 0);
insert into Phong values ('G193', 3, 'Phòng VIP', 0);
insert into Phong values ('G194', 1, 'Phòng thường', 0);
insert into Phong values ('G195', 2, 'Phòng family', 0);
insert into Phong values ('G196', 3, 'Phòng VIP', 0);
insert into Phong values ('G198', 1, 'Phòng thường', 0);
insert into Phong values ('G199', 2, 'Phòng family', 0);
insert into Phong values ('G200', 3, 'Phòng VIP', 0);


--Khách Hàng--
insert into KhachHang(TenKhachHang,MaLoaiKhach,DiaChi,CMND) values ('Gray',1,'123 adbcsd','123456789')
insert into KhachHang(TenKhachHang,MaLoaiKhach,DiaChi,CMND) values ('John Cena',1,'456 adbcsd','123456789')
insert into KhachHang(TenKhachHang,MaLoaiKhach,DiaChi,CMND) values ('Siscon',2,'789 adbcsd','123456789')
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Jouannisson', 1, '86589 Luster Street', '129798261-4');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Downgate', 1, '369 Milwaukee Point', '281676795-7');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Kingsworth', 1, '469 Meadow Valley Circle', '799614212-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Mossdale', 2, '865 Autumn Leaf Center', '433859003-3');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('d'' Elboux', 2, '94011 Drewry Hill', '917636200-0');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Farlambe', 1, '68158 Marquette Trail', '336738617-0');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Marchent', 2, '921 Killdeer Alley', '171222404-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Di Ruggiero', 1, '27 Talmadge Parkway', '812614259-6');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Nanelli', 1, '30 Derek Street', '327570536-9');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Ellwood', 1, '99980 Spaight Terrace', '960425799-4');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Leckie', 2, '877 Crescent Oaks Way', '612724924-4');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Sibbering', 1, '86 John Wall Trail', '854778751-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Winyard', 1, '8114 Manley Crossing', '373950602-4');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Stapley', 1, '4 Burrows Center', '246152384-3');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Patel', 2, '6621 Fairview Junction', '356057712-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Nutbrown', 1, '95949 Troy Hill', '671898564-3');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Wescott', 1, '9246 Northfield Trail', '227860117-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Flucker', 2, '199 Ridgeway Street', '349292525-1');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Gavagan', 1, '89283 Straubel Park', '316127283-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Pinkett', 2, '3005 Algoma Road', '387673035-X');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Langfield', 2, '904 Marcy Point', '234919303-9');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Messier,', 1, '3 Division Crossing', '742594793-5');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Aven', 2, '1 Lawn Crossing', '235574751-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Freak', 1, '42 Hooker Place', '462918556-9');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Le Brum', 2, '33388 Mayer Court', '021635505-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Braiden', 2, '69650 Fieldstone Parkway', '757047201-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Pessolt', 1, '86 Hauk Hill', '538595307-5');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Doubrava', 2, '4 Banding Crossing', '575397677-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Gumm', 2, '4 Stoughton Trail', '117212052-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Aindriu', 1, '2048 Nobel Drive', '883780824-0');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Spinelli', 2, '70 Pine View Point', '505895269-6');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Santos', 2, '4 Ridgeview Avenue', '799041848-7');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Grzesiak', 2, '955 Kenwood Parkway', '441447534-1');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Skowcraft', 2, '7055 Atwood Way', '703235009-7');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Costy', 2, '0557 Sachs Plaza', '380921941-X');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Gorham', 1, '22202 Jana Hill', '995764549-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Gogay', 1, '8 Rigney Center', '113386985-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Reucastle', 2, '9 Lakewood Trail', '487552372-6');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('St. Clair', 2, '2 Hagan Center', '909058446-3');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Martinovsky', 2, '1992 Del Mar Road', '015354011-7');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Fenner', 2, '53 Tennyson Lane', '757665280-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Eastop', 2, '7686 Holmberg Pass', '810534689-3');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Beteriss', 2, '7638 Rowland Park', '687048393-0');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Griston', 2, '8 Service Crossing', '080954720-1');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('McRory', 1, '92203 Carey Place', '483678743-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Huygen', 2, '05 Arizona Place', '138373238-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Goreisr', 2, '71115 Sutteridge Parkway', '094175062-0');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Nickell', 2, '5 Delladonna Lane', '928071733-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Carley', 2, '1128 Westend Point', '487242645-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Maneylaws', 2, '60997 Burning Wood Way', '626459501-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Conrard', 2, '83 Washington Place', '488753348-9');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Sickert', 1, '4913 Quincy Street', '191724394-4');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Sculley', 2, '66 American Center', '099174291-5');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Ramsay', 1, '710 Talmadge Place', '855712567-4');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Mordy', 1, '36 North Trail', '155257444-X');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('O''Corren', 2, '9 High Crossing Junction', '549129714-3');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Wyard', 1, '84 Grasskamp Street', '251628089-0');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Fattorini', 1, '0 Chive Center', '609610241-7');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Raybould', 1, '1155 Kenwood Crossing', '234898742-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Norvel', 2, '2 Upham Hill', '219420710-4');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Studders', 1, '5618 Green Ridge Drive', '414413207-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Cridlan', 2, '8656 Maple Wood Trail', '648158379-9');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Ludgate', 1, '1618 Bultman Terrace', '225616401-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Axston', 1, '6929 Steensland Alley', '560202860-9');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Kowalski', 2, '9101 Sachs Street', '804963994-0');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Luckman', 2, '92 Golf Course Alley', '043938135-5');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Swannie', 1, '27519 Eagle Crest Avenue', '470462451-X');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Fearns', 2, '2750 Dexter Avenue', '514161126-4');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Fairholme', 2, '291 Amoth Junction', '216601268-X');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Sarah', 1, '361 Warner Center', '270476968-0');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Naden', 2, '29958 Everett Junction', '670435632-0');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Underwood', 2, '3 Menomonie Drive', '334417934-9');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Truesdale', 1, '5 Pierstorff Pass', '526251958-X');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Draijer', 2, '3466 Esker Avenue', '610304967-9');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Burel', 2, '790 Eggendart Avenue', '179955237-3');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Barnbrook', 1, '45166 Summerview Avenue', '633487903-0');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Scala', 2, '52 Sauthoff Way', '097192160-1');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Ferdinand', 1, '1462 Menomonie Way', '266963868-6');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Simionescu', 1, '864 Atwood Trail', '906091932-7');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Ivakin', 2, '634 Logan Alley', '741957834-6');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Dehn', 1, '6 Montana Crossing', '236685459-5');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Brunn', 1, '34 Pond Road', '006026346-6');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Hawick', 2, '42420 Texas Road', '558106192-0');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Cannam', 2, '39005 Fairfield Trail', '281738302-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Balden', 2, '8 Schurz Center', '967568957-9');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Sleford', 1, '86 Cody Place', '792447945-7');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Casford', 2, '579 Service Drive', '570860939-7');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Towne', 2, '9 Muir Court', '729440867-X');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Daniaud', 2, '4279 Clyde Gallagher Trail', '531674569-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Stapels', 2, '09338 Beilfuss Plaza', '371467043-2');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Weston', 2, '278 Menomonie Terrace', '146648414-4');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Tugwell', 1, '94609 Pearson Pass', '790023345-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Fishburn', 1, '8116 Granby Street', '560792650-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Heams', 2, '9 Surrey Center', '138833940-4');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Coggles', 2, '471 Mesta Drive', '800600869-8');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Benford', 2, '0 Merrick Avenue', '377293249-5');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Crannis', 2, '22 Canary Junction', '232513289-7');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Miguel', 2, '2330 Jenna Crossing', '863305992-4');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Zoppo', 1, '35199 Pond Place', '363919722-4');
insert into KhachHang (TenKhachHang, MaLoaiKhach, DiaChi, CMND) values ('Brimman', 2, '291 Annamark Court', '075371571-6');


--ThuePhong--
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Ii8t', 'P101', '2016-05-23 16:49:48', '37808-698', 'Nuoc Ngoai',0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Treefluex', 'P2', '2016-08-28 00:21:16', '16477-505', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Domaiuiner', 'P3', '2016-09-08 18:11:10', '51444-003', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Tres-ooZap', 'P4', '2016-07-26 13:02:25', '51672-4051', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Coo87kley', 'P5', '2017-03-05 16:17:44', '24658-291', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Mat L6am T8am', 'P6', '2016-08-25 19:13:56', '0168-0449', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bitw887olf', 'P7', '2016-11-16 19:59:26', '0378-0085', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Nam8fix', 'P8', '2016-07-05 22:28:59', '76237-275', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Z8amit', 'P9', '2016-08-04 23:53:27', '49349-891', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('It3', 'P10', '2016-05-14 02:58:15', '68682-525', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('It2', 'P11', '2017-03-07 23:30:17', '64108-313', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('It1', 'P12', '2016-07-14 14:43:44', '37205-031', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Voyato575uch', 'P13', '2016-07-17 11:05:52', '51346-137', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Fixfle87x', 'P14', '2016-08-23 19:31:43', '62191-004', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Viv787a', 'P15', '2016-05-06 08:51:19', '0363-8410', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Zaam-D87ox', 'P16', '2016-11-27 22:33:20', '11673-090', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bit65wolf', 'P17', '2016-10-18 23:08:45', '61314-044', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('87787878', 'P18', '2016-12-20 05:14:34', '52959-034', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Lotst76ring', 'P19', '2016-07-23 20:07:18', '21130-076', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Konklu65x', 'P20', '2016-12-11 08:40:00', '64525-0542', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Job45', 'P21', '2016-05-28 01:49:31', '57520-0251', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Strong-0hold', 'P22', '2016-06-28 09:59:54', '62011-0013', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Str09onghold', 'P23', '2017-02-15 22:12:57', '58118-0019', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Rons999tring', 'P24', '2017-03-06 23:04:12', '64011-243', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Lotl9ux', 'P25', '2016-04-06 07:13:41', '55154-4619', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Fint8one', 'P26', '2016-11-04 11:52:17', '42936-585', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Ha8tity', 'P27', '2016-07-28 05:32:19', '13811-050', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Treeflex', 'P28', '2016-07-18 05:38:34', '49999-060', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Cookley', 'P29', '2016-12-12 22:12:00', '49614-448', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Tempsoft', 'P30', '2016-08-01 23:28:37', '56062-041', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Ventosanzap', 'P31', '2016-07-17 14:23:13', '36800-419', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Home Ing', 'P32', '2016-05-30 04:06:47', '55154-7807', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Tampfl636ex', 'P33', '2016-04-08 19:27:56', '47124-720', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Cardgu3ard', 'P34', '2016-09-18 02:30:44', '51655-627', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Vivra', 'P35', '2016-04-06 23:48:57', '0440-1655', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Regrant', 'P36', '2017-01-03 05:15:21', '51672-5281', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Y-trfind', 'P37', '2016-04-15 09:33:27', '55301-368', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Prodgd54er', 'P38', '2017-03-07 19:41:37', '42507-198', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Vifva', 'P39', '2016-11-14 06:34:42', '36987-2316', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Biodwex', 'P40', '2016-05-03 18:16:15', '49738-990', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Zoola', 'P41', '2016-11-10 05:07:33', '35356-582', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Sonsing', 'P42', '2016-08-11 10:47:12', '0406-9906', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Asok0a', 'P43', '2017-01-08 10:43:46', '36987-1213', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Sola09rbreeze', 'P44', '2016-04-24 12:06:00', '60760-168', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Cardgeuard', 'P45', '2016-06-22 10:48:27', '0517-5602', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Coorkley', 'P46', '2017-02-09 22:51:45', '64980-157', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Vagram', 'P47', '2016-11-17 11:58:47', '12745-177', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Stim', 'P48', '2016-12-09 07:55:17', '54868-5600', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('TresZap', 'P49', '2016-06-19 12:10:02', '24385-390', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Fixfelex', 'P50', '2016-08-26 01:41:16', '50972-273', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Fi09x San', 'P51', '2016-06-15 07:54:40', '61314-016', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bigt09ax', 'P52', '2016-05-06 09:10:04', '48951-3002', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Asokea', 'P53', '2016-10-05 07:45:03', '0378-0460', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Byteca0909rd', 'P54', '2016-12-15 00:13:12', '64127-164', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Fixfrlex', 'P55', '2016-09-23 23:28:12', '68788-0012', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Redh09old', 'P56', '2016-11-25 02:45:25', '35356-702', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Zat98hin', 'P57', '2016-10-21 15:26:03', '24236-995', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Aerif98ied', 'P58', '2016-09-28 04:59:39', '51824-019', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('6676', 'P59', '2016-09-26 02:56:15', '65862-133', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Volts77illam', 'P60', '2016-05-19 10:09:51', '57955-7304', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Gembuc87ket', 'P61', '2016-10-25 00:46:22', '59762-0066', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Redhorld', 'P62', '2016-11-21 09:00:40', '52125-746', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Hatit7y', 'P63', '2016-10-02 11:37:09', '49520-101', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Gejmbpucket', 'P64', '2016-10-17 19:38:38', '0409-2066', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Finrtone', 'P65', '2016-05-18 08:17:52', '0409-7118', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Namftix', 'P66', '2016-05-07 11:35:33', '51613-002', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Pann7ier', 'P67', '2016-08-28 09:19:48', '10237-800', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Pannoier', 'P68', '2016-11-26 16:06:53', '0591-0844', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Treeoflex', 'P69', '2016-08-19 08:36:15', '49520-502', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Temposoft', 'P70', '2016-08-22 04:53:11', '24470-901', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bitwolof', 'P71', '2016-11-14 22:37:27', '63824-455', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Stronoghold', 'P72', '2017-02-13 01:16:40', '42361-035', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Y-Sol75owarm', 'P73', '2016-11-02 18:53:13', '0603-7642', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Latl7ux', 'P74', '2016-08-05 04:00:51', '41520-911', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Voytouch', 'P75', '2016-05-27 04:23:06', '54868-5575', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Ventosaap', 'P76', '2016-07-11 13:30:42', '48663-0001', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Joob', 'P77', '2016-06-20 07:56:28', '0024-0590', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Yofind', 'P78', '2016-10-10 20:35:54', '0268-0248', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Zathion', 'P79', '2016-07-26 18:54:00', '37000-502', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Viova', 'P80', '2017-02-02 13:24:16', '61727-050', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Lootstring', 'P81', '2017-03-10 00:26:14', '49260-716', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Wrapsa7fe', 'P82', '2016-05-08 05:17:48', '21695-831', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Zatyhyin', 'P83', '2017-03-29 23:06:04', '0378-2224', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Fintyone', 'P84', '2016-11-20 22:58:30', '16714-025', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('T7in', 'P85', '2016-08-07 15:20:42', '0169-7202', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Otco77m', 'P86', '2017-01-05 20:30:13', '11822-0126', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Green77lam', 'P87', '2016-10-24 02:00:00', '55312-104', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Zooylab', 'P88', '2016-10-21 07:11:39', '11523-7306', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bigtyax', 'P89', '2016-10-26 23:49:58', '49738-094', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Byteycard', 'P90', '2016-11-11 18:39:13', '55885-010', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Son86air', 'P91', '2016-07-03 19:04:18', '43419-029', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Quo 86Lux', 'P92', '2016-07-26 01:52:15', '63776-610', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Panynier', 'P93', '2016-07-12 06:55:27', '43419-016', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Transco55f', 'P94', '2016-08-19 04:30:23', '65862-193', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Kanl86am', 'P95', '2017-02-22 09:40:25', '67938-1076', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Asoyka', 'P96', '2016-07-22 05:19:18', '11673-612', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bigt86yax', 'P97', '2016-10-13 10:47:37', '57955-0173', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Laytlux', 'P98', '2017-02-26 15:55:29', '35356-914', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Alphazap', 'P99', '2016-10-26 02:15:16', '50246-100', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Toughjoyfax', 'P100', '2017-01-17 21:34:46', '62175-382', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Proddeyr', 'P101', '2016-07-18 23:23:21', '63783-015', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Fixfleyx', 'P102', '2016-12-08 10:44:41', '60681-0011', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Asyoka', 'P103', '2016-05-27 09:42:32', '0378-1125', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Tranyscof', 'P104', '2016-10-01 00:41:02', '16590-018', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Zontra65x', 'P105', '2017-02-15 23:39:45', '54569-6044', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Gembuycket', 'P106', '2016-06-22 12:29:38', '55648-974', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Biytwolf', 'P107', '2017-03-09 02:12:11', '52584-077', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Subin56', 'P108', '2016-07-06 13:58:06', '53329-981', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('HomeyIng', 'P109', '2016-10-03 16:46:15', '54235-205', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bytecayrd', 'P110', '2017-01-10 15:10:04', '76281-301', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Iyt', 'P111', '2016-06-11 12:20:45', '68276-004', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Touguhjoyfax', 'P112', '2016-04-26 07:33:14', '50387-100', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Gemubuicket', 'P113', '2016-04-20 03:25:55', '50014-100', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Solareeze', 'P114', '2017-03-02 16:32:53', '54868-4744', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Vensanzap', 'P115', '2016-12-01 22:53:11', '46122-192', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('HoIng', 'P116', '2016-08-28 14:58:46', '63187-090', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Gembuct', 'P117', '2017-03-29 22:58:10', '68001-185', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Redihold', 'P118', '2016-10-02 09:34:59', '68084-128', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Zontirax', 'P119', '2016-04-16 00:23:43', '35356-804', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Domaiiner', 'P120', '2016-08-23 09:54:25', '42908-075', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Alphazoap', 'P121', '2016-10-05 20:47:29', '55312-252', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Tougohjoyfax', 'P122', '2016-09-20 14:01:50', '46123-002', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Kanolam', 'P123', '2016-11-20 20:40:21', '50382-051', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Biodpex', 'P124', '2017-02-19 08:20:08', '41520-345', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Zathi76n', 'P125', '2016-07-01 14:05:56', '43742-0499', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Greepnlam', 'P126', '2017-01-25 09:29:39', '11523-7200', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Daltfregsh', 'P127', '2016-09-08 14:44:10', '52533-071', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Voyatofuch', 'P128', '2016-12-14 02:38:41', '68345-850', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Ronstdring', 'P129', '2016-10-01 19:12:54', '0363-0560', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Proddder', 'P130', '2017-03-17 14:06:08', '48951-9034', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Regsrant', 'P131', '2016-08-06 09:36:42', '46122-259', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bitchip87', 'P132', '2017-01-22 03:47:10', '67777-217', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Sonssing', 'P133', '2016-06-06 08:05:30', '98132-664', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Alphdazap', 'P134', '2016-09-28 15:15:29', '17772-103', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Fix Sdan', 'P135', '2017-03-07 21:02:06', '61957-0112', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('MatTam', 'P136', '2017-02-19 03:08:17', '0378-7010', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bitwodlf', 'P137', '2017-02-09 05:00:11', '24385-677', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bytecddard', 'P138', '2016-11-09 02:03:59', '10191-1909', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Zaamrox', 'P139', '2016-12-18 01:20:53', '49035-376', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Subrn', 'P140', '2016-09-20 10:31:57', '64725-0259', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Greeram', 'P141', '2016-09-22 04:55:37', '60429-107', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Regrrant', 'P142', '2017-01-26 22:58:43', '49349-347', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Veri78bet', 'P143', '2016-05-13 04:58:19', '52000-014', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Treeftlex', 'P144', '2016-04-14 20:17:06', '0220-9131', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Treeftttlex', 'P145', '2016-10-17 11:08:49', '68001-205', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bamit7y', 'P146', '2017-04-02 15:20:22', '41250-315', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Alpha7', 'P147', '2016-06-21 22:53:58', '0781-1830', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('rrrr', 'P148', '2016-10-04 17:47:53', '98132-902', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Gembutttcket', 'P149', '2017-03-26 15:47:17', '0268-1026', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bitcthip', 'P150', '2016-09-08 11:55:23', '17478-069', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Kanlattm', 'P151', '2017-02-20 20:42:46', '51861-010', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('tttt', 'P152', '2016-07-04 20:02:36', '16729-005', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Konkab', 'P153', '2016-05-23 06:55:54', '55289-725', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Ronsttring', 'P154', '2016-10-10 11:33:39', '63354-291', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Zatthin', 'P155', '2017-03-17 03:50:50', '13668-084', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Fix tSan', 'P156', '2016-11-02 23:34:12', '53329-015', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Vivta', 'P157', '2016-12-17 14:56:06', '76140-106', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Regtrant', 'P158', '2016-11-28 10:53:50', '60429-621', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Redholdtt', 'P159', '2016-08-17 06:25:31', '0904-6073', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Toughjoyfatx', 'P160', '2016-06-21 18:45:27', '0280-1460', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Lattlux', 'P161', '2016-05-24 18:49:50', '0781-2263', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Trippledrex', 'P162', '2016-08-19 11:00:28', '43419-302', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Greenlamrr', 'P163', '2016-08-20 12:23:32', '49349-205', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('rrtrtr', 'P164', '2016-04-24 02:39:49', '0245-0014', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Alphatzap', 'P165', '2016-11-19 04:43:16', '54868-1804', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Lotstritng', 'P166', '2016-05-26 20:36:46', '42549-674', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Sub-E87x', 'P167', '2016-05-10 15:35:40', '58443-0021', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Tempsrroft', 'P168', '2016-07-08 02:08:40', '53489-609', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Stronrrrghold', 'P169', '2017-01-31 10:57:20', '0268-0921', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('7878', 'P170', '2016-05-30 02:38:26', '55714-4539', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bitchtttip', 'P171', '2017-03-31 13:22:46', '68084-337', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Op6ela', 'P172', '2016-08-20 18:05:17', '52244-010', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Trest-Zap', 'P173', '2017-04-01 09:00:49', '13537-030', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('ZaamrDox', 'P174', '2016-11-17 14:12:37', '43419-547', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Zamitt', 'P175', '2016-10-24 03:52:48', '50383-901', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Stimr', 'P176', '2016-10-26 00:17:32', '41250-447', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Keylexr', 'P177', '2016-11-29 15:12:02', '54575-359', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Wraprsafe', 'P178', '2016-09-21 21:26:06', '66467-3730', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Kanlrram', 'P179', '2016-04-14 10:59:29', '68258-7128', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Tem6p', 'P180', '2016-04-24 23:15:27', '59735-100', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Quo tLux', 'P181', '2016-08-14 10:23:24', '51367-017', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Bit6cthip', 'P182', '2016-08-27 22:42:11', '51991-362', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Matttam', 'P183', '2016-08-19 12:31:20', '60429-221', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Proddter', 'P184', '2016-05-10 09:10:58', '68084-619', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Cardgtuard', 'P185', '2016-12-30 08:52:39', '55670-143', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Ottcom', 'P186', '2016-09-19 23:50:35', '43419-310', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Fix trSan', 'P187', '2016-04-21 00:15:03', '52685-448', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Getmbucket', 'P188', '2016-05-29 02:50:50', '54868-3722', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Konktlux', 'P189', '2016-11-19 18:41:50', '41520-397', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Voyatoucrh', 'P190', '2017-02-15 05:01:22', '53057-016', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Vivrrta', 'P191', '2017-02-09 21:54:51', '50563-301', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Hattity', 'P192', '2016-06-24 00:20:25', '60512-1010', 'Trong Nuoc', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Optela', 'P193', '2016-06-13 20:48:45', '50563-137', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Flexidpy', 'P194', '2017-03-04 11:36:14', '47781-370', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Greenlatrtrm', 'P195', '2016-07-07 22:15:11', '36987-2211', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Vagerteram', 'P196', '2016-08-24 05:50:01', '0074-7804', 'Trong Nuoc', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Strretreingtough', 'P197', '2016-08-14 20:12:20', '52125-373', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Venttretosanzap', 'P198', '2017-02-15 13:37:47', '13537-337', 'Nuoc Ngoai', 1);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Solartretbreeze', 'P199', '2017-01-27 17:44:38', '64942-1101', 'Nuoc Ngoai', 0);
insert into ThuePhong (MaThuePhong, MaPhong, NgayBatDauThue, CMND, LoaiKhach, TinhTrang) values ('Cooktretley', 'P200', '2017-02-22 21:14:11', '14537-408', 'Trong Nuoc', 1);
SELECT * from Phong where TinhTrang Like '%0%';
EXEC sp_MSforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"
exec sp_MSforeachtable @command1="print '?'", @command2="ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"
go

